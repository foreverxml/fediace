# Fediace

[Fediace](https://fedi.aroace.space) is a complete frontend rework for [GoToSocial](https://gts.superseriousbusiness.org) which makes some things different, including:

- Margins minified
- Toots in a thread/profile view will collapse borders
- No box shadow: instead, a border is used
- Media displayment improvements
- Admin panel improvements
- Display name is no longer overlayed on header image
- Monospace font and sepia images for ease on eyes
- Description (not short description) is shoen on the front page
- Renamed posts to *pings*
- Links to [Fediace Pinafore](https://client.fedi.aroace.space)

And more to come in the future:
- Webpage title changes based on location in website

## How to use

Simply copy the `web/` folder to your GoToSocial folder and restart GoToSocial.

## Set up from scratch
You will need `caddy` and `git` installed. In root shell:
```sh
cd /
git clone https://codeberg.org/foreverxml/fediace
cd fediace
cp example/config.yaml config.yaml
# edit config.yaml, adding in your instance url.
nano config.yaml
# then put that same one in the Caddyfile
nano Caddyfile
./gotosocial --config-path ./config.yaml admin account create --username FediaceUser --password ThisIsMyMostExcellentPassword --email email@example.org
# please swap those out
./gotosocial --config-path ./config.yaml admin account confirm --username FediaceUser
./gotosocial --config-path ./config.yaml admin account promote --username FediaceUser
# then start the server. use this command to stsrt it now
./gotosocial --config-path ./config.yaml server start & && disown
caddy start
```
## Licensing

It is reccomended that you modify this repo! However, you must follow the terms of the AGPL and publish it on a software forge.
